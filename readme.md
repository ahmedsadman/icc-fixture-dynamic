# ICC Dynamic Fixture #
This is a dynamic fixture of ICC Cricket World Cup 2015, made with HTML/CSS and JQuery. It can
keep record of the game statistics and calculate points

## Contributions ##
Feel free to contribute in the code, improve it, add more features and anything you like. But
you are required to give proper attributions when distributing (see **license.txt**)

## License ##
Released under MIT License. 
(C) Sadman Muhib Samyo - 2015
